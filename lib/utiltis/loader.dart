import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';

const spinkitCircle = SpinKitRotatingCircle(
  color: Colors.white,
  size: 80.0,
);
const spinkitPlus = SpinKitPulse(
  color: Colors.amber,
  size: 80.0,
);