import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weathermap_quiz/bloc/weathers/brands_bloc.dart';
import 'package:weathermap_quiz/bloc/weathers/brands_event.dart';
import 'package:weathermap_quiz/bloc/weathers/brands_state.dart';
import 'package:weathermap_quiz/models/base_model.dart';
import 'package:weathermap_quiz/utiltis/loader.dart';
import 'package:weathermap_quiz/utiltis/size_config.dart';
class WeatherDetalisScreen extends StatefulWidget {
  final DateTime time;

  const WeatherDetalisScreen({Key key, this.time}) : super(key: key);
  @override
  _WeatherDetalisScreenState createState() => _WeatherDetalisScreenState();
}

class _WeatherDetalisScreenState extends State<WeatherDetalisScreen> {
  Bloc _blocWeathers;
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if (_blocWeathers == null) {
      _blocWeathers = BlocProvider.of<WeathersBloc>(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    _blocWeathers.add(GetWeathers(date:widget.time));
    return Scaffold(
      body:BlocBuilder<WeathersBloc,WeathersState>(
          builder: (_, WeathersState state) {
            if (state is LoadingWeathersState) {
              return Center(child:Container(height:100,child: spinkitPlus));
            }
            else if (state is LoadedWeathersState) {
              return buildWeather(state.weathers);
            }
            else {
              return buildError();
            }
          }
      ),
    );
  }
  check(int deg){
    if(deg>=32){
      return "assets/images/sunny.jpg";
    }else if(deg<32 || deg>=22){
      return "assets/images/cloudy.jpg";
    }else if(deg<=18){
      return "assets/images/running.jpg";
    }
  }
  buildWeather(General general){
    print(general.temperatures.length);
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(check((general.temperatures[0].main.temp-273.15).ceil())),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.all(5.0*SizeConfig.heightMultiplier),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(general.temperatures[0].weathers[0].main,style: TextStyle(color: Colors.white,fontSize: SizeConfig.textMultiplier*3.5),),
                  Text(general.temperatures[0].weathers[0].description,style: TextStyle(color: Colors.white,fontSize: SizeConfig.textMultiplier*3.5),),
                ],
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.all(5.0*SizeConfig.heightMultiplier),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(general.city.name,
                    style: TextStyle(color: Colors.white,fontSize: SizeConfig.textMultiplier*5.0),),
                  Row(
                    children: <Widget>[
                      Text( (general.temperatures[0].main.temp-273.15).ceil().toString(),
                        style: TextStyle(color: Colors.white,fontSize: SizeConfig.textMultiplier*8.0),),
                      Container(
                        padding: EdgeInsets.only(left: SizeConfig.textMultiplier),
                        height: SizeConfig.heightMultiplier*6,
                        child: ListView(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            Container(color: Colors.white,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                Text("Speed"),
                                Text(general.temperatures[0].wind.speed.toString()+'K/h')],),),
                            SizedBox(width: 100,),
                            Container(color: Colors.white,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                Text("degree"),
                                Text(general.temperatures[0].wind.deg.toString()+'%')],),)
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),)
        ],
      ) /* add child content here */,
    );
  }
  Widget buildError() {
    return Center(
      child: Column(
        children: <Widget>[
          Text("please re-try agin"),
          IconButton(
            icon: Icon(
              Icons.refresh,
              size: 40,
              color: Colors.black,
            ),
            onPressed: () {
              _blocWeathers.add(GetWeathers());
            },
          ),
        ],
      ),
    );
  }
}
