
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weathermap_quiz/bloc/weathers/brands_bloc.dart';
import 'package:weathermap_quiz/bloc/weathers/brands_event.dart';
import 'package:weathermap_quiz/repository/weather_repositories.dart';
import 'package:weathermap_quiz/screens/weather_detalis_screen.dart';
import 'package:weathermap_quiz/utiltis/size_config.dart';

class WeatherScreen extends StatefulWidget {
  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    return  SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            InkWell(
              onTap: (){

                Navigator.of(context).push(
                  MaterialPageRoute<WeatherDetalisScreen>(
                    builder: (_) => BlocProvider.value(
                      value: BlocProvider.of<WeathersBloc>(context),
                      child: WeatherDetalisScreen(time:now),
                    ),
                  ),
                );

              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(topLeft:Radius.circular(50.0),bottomRight: Radius.circular(50.0))
                ),
                width: double.infinity,
                height: SizeConfig.imageSizeMultiplier*50,
                child: Center(
                    child: Text("Today",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.textMultiplier*3.5),textAlign: TextAlign.center,)),
              ),
            ),
            SizedBox(height: SizeConfig.heightMultiplier*4,),
            InkWell(
              onTap: (){
                final tomorrowNoon = new DateTime(now.year, now.month, now.day + 1, 12);
                Navigator.of(context).push(
                  MaterialPageRoute<WeatherDetalisScreen>(
                    builder: (_) => BlocProvider.value(
                      value: BlocProvider.of<WeathersBloc>(context),
                      child: WeatherDetalisScreen(time:tomorrowNoon),
                    ),
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.orangeAccent,
                    borderRadius: BorderRadius.only(topLeft:Radius.circular(50.0),bottomRight: Radius.circular(50.0))
                ),
                width: double.infinity,
                height: SizeConfig.imageSizeMultiplier*50,
                child: Center(child: Text(
                  "Tomorow",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: SizeConfig.textMultiplier*3.5),)),
              ),
            ),
            SizedBox(height: SizeConfig.heightMultiplier*4,),
            InkWell(
              onTap: (){
                final next = new DateTime(now.year, now.month, now.day + 2, 12);
                Navigator.of(context).push(
                  MaterialPageRoute<WeatherDetalisScreen>(
                    builder: (_) => BlocProvider.value(
                      value: BlocProvider.of<WeathersBloc>(context),
                      child: WeatherDetalisScreen(time:next),
                    ),
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.only(topLeft:Radius.circular(50.0),bottomRight: Radius.circular(50.0))
                ),
                width: double.infinity,
                height: SizeConfig.imageSizeMultiplier*50,
                child: Center(child: Text("The Next",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: SizeConfig.textMultiplier*3.5),)),
              ),
            ),
          ],
        ),
      ),
    );
  }

}
