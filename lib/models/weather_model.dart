import 'package:weathermap_quiz/models/city_model.dart';
import 'package:weathermap_quiz/models/temperature_model.dart';


class Weather{
  final int id;
  final String main;
  final String description;
  final String icon;
  Weather({this.id, this.main, this.description, this.icon});
  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
        id:json['id'],
        main: json['main'],
        description: json['description'],
        icon: json['icon'],
    );
  }
}



