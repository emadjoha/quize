import 'package:intl/intl.dart';
import 'package:weathermap_quiz/models/main_model.dart';
import 'package:weathermap_quiz/models/weather_model.dart';
import 'package:weathermap_quiz/models/wind_model.dart';

import 'clouds_model.dart';

class Temperature{
  final int dt;
 // final Main main;
 final List<Weather> weathers;
  final Clouds clouds;
  final int visibility;
  final String dt_txt;
  final MainX main;
  final Wind wind;
  Temperature({this.dt, this.visibility,this.wind,this.main, this.dt_txt,this.weathers,this.clouds});
  factory Temperature.fromJson(Map<String, dynamic> json) {
    return Temperature(
      dt:json['dt'],
     main: MainX.fromJson(json['main']),
      weathers:json['weather'] != null
          ? json['weather'].map<Weather>((json) => Weather.fromJson(json)).toList()
          : null,
     clouds: Clouds.fromJson(json['clouds']),
      wind:Wind.fromJson(json['wind']),
      visibility: json['visibility'],
      dt_txt: json['dt_txt']
    );
  }

}
