import 'package:weathermap_quiz/models/temperature_model.dart';

import 'city_model.dart';

class General {
  final String code;
  final int message;
  List<Temperature> temperatures;
  final City city;
  General({this.city,this.code, this.message,this.temperatures});
  factory General.fromJson(Map<String, dynamic> json) {
    return General(
        code:json['code'],
        message: json['message'],
        city:City.fromJson(json['city']),
        temperatures: json['list'] != null
            ? json['list'].map<Temperature>((json) =>  Temperature.fromJson(json)).toList()
            : null,
    );
  }
}