class City {
  final int id;
  final String name;
  final String country;
  final int population;
  final int timeZone;
  final int sunRise;
  final int sunSet;
  final Coordinator coordinator;
  City({this.id, this.name, this.country, this.population, this.timeZone, this.sunRise, this.sunSet,this.coordinator});
  factory City.fromJson(Map<String, dynamic> json) {
    return City(
      id:json['id'],
      name: json['name'],
      country: json['country'],
      population: json['population'],
      timeZone: json['timezone'],
      sunRise: json['sunrise'],
      sunSet: json['sunset'],
      coordinator: Coordinator.fromJson(json['coord'])
    );
  }
}

class Coordinator {
  final double latitude;
  final double longitude;
  Coordinator({this.latitude, this.longitude});
  factory Coordinator.fromJson(Map<String, dynamic> json) {
    return Coordinator(
        latitude:json['lat'],
        longitude: json['lon'],
    );
  }
}