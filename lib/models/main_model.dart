class MainX {
  final double temp;
  final double feels_like;
  final double temp_min;
  final double temp_max;
  final int pressure;
  final int sea_level;
  final int grnd_level;
  final int humidity;
 // final double temp_kf;
  MainX({this.temp, this.feels_like, this.temp_min,
    this.temp_max, this.pressure, this.sea_level,
    this.grnd_level, this.humidity,});
  factory MainX.fromJson(Map<String, dynamic> json) {
   return MainX(
          temp:double.parse(json['temp'].toString()),
          feels_like: double.parse(json['feels_like'].toString()),
          temp_min: double.parse(json['temp_min'].toString()),
          temp_max: double.parse(json['temp_max'].toString()),
          pressure: json['pressure'],
          sea_level: json['sea_level'],
          grnd_level: json['grnd_level'],
          humidity: json['humidity'],
          //temp_kf: json['temp_kf']
      );
  }
}