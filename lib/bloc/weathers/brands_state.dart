import 'package:equatable/equatable.dart';
import 'package:weathermap_quiz/models/base_model.dart';


abstract class WeathersState extends Equatable {
  const WeathersState();
}


class LoadingWeathersState extends WeathersState {
  const LoadingWeathersState();

  @override
  List<Object> get props => [];
}

class LoadedWeathersState extends WeathersState {
  final General weathers;
  LoadedWeathersState({this.weathers});
  @override
  List<Object> get props => [weathers];
}

class WeathersError extends WeathersState {
  final String message;

  const WeathersError({this.message});

  @override
  List<Object> get props => [message];
}
