import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:weathermap_quiz/bloc/weathers/brands_event.dart';
import 'package:weathermap_quiz/bloc/weathers/brands_state.dart';
import 'package:weathermap_quiz/repository/weather_repositories.dart';


class WeathersBloc extends Bloc<WeathersEvent, WeathersState> {
  final WeatherRepo repository;

  WeathersBloc({this.repository}) : super(LoadingWeathersState());


  @override
  Stream<WeathersState> mapEventToState(WeathersEvent event,) async* {
    // TODO: Add Logic'
    yield LoadingWeathersState();
    if (event is GetWeathers) {
      try {
        final weathers = await repository.weathers(event.date);
        yield LoadedWeathersState(weathers: weathers);
      }on Exception catch (error) {
        yield WeathersError(message: error.toString());
      }
    }

  }
}
