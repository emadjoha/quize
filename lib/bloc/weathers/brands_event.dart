import 'package:equatable/equatable.dart';

abstract class WeathersEvent extends Equatable {
  const WeathersEvent();
}


class GetWeathers extends WeathersEvent{
  final DateTime date;
  GetWeathers({this.date});
  @override
  List<Object> get props => [date];
}