import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:weathermap_quiz/constants/urls_contants.dart';
import 'package:weathermap_quiz/models/base_model.dart';


class WeatherRepo {
  static final WeatherRepo _instance = WeatherRepo._internal();
  factory WeatherRepo() {
    return _instance;
  }
  WeatherRepo._internal();

  Future<General> weathers(DateTime dateTime) async {
    try {

      final response = await http.get(Url.BASE).
      timeout(Duration(seconds: 8),onTimeout:() => throw Exception());
      final responseBody = json.decode(response.body);
      String success =  json.decode(response.body)['cod'];
      if(response.statusCode==200){
        if(success!='200')return throw Exception("Failed to load");
        final DateFormat formatter = DateFormat('yyyy-MM-dd');
        final String formatted = formatter.format(dateTime);
          return General.fromJson(responseBody);
      }else{
        throw Exception("Failed to load");
      }
    } catch (err) {
      print(err);
      throw err;
    }
  }
}
WeatherRepo weatherRepo = WeatherRepo();