import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weathermap_quiz/bloc/weathers/brands_bloc.dart';
import 'package:weathermap_quiz/models/weather_model.dart';
import 'package:weathermap_quiz/repository/weather_repositories.dart';
import 'package:weathermap_quiz/screens/weather_screen.dart';
import 'package:weathermap_quiz/utiltis/size_config.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather App',
      theme: ThemeData(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (context, constraints) {
          return OrientationBuilder(
            builder: (context, orientation) {
              SizeConfig().init(constraints, orientation);
              return MaterialApp(
                debugShowCheckedModeBanner: false,
                home:Scaffold(
                  appBar: AppBar(title:Text('Weather App'),),
                    body: Container(
                        child:  MultiBlocProvider(
                          providers: [
                             BlocProvider<WeathersBloc>(
                        create: (_) => WeathersBloc(repository: WeatherRepo()),
                             ),
                          ],
                        child:WeatherScreen() ,
                        )
                    )
                ),
              );
            },
          );
        }
    );
  }
}
